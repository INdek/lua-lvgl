local cparser = require 'CParser.cparser'

local enums = {}
local defines = {}
local modules = {}
local typedefs = {}

-- If a type is a typedef we return the base type
-- otherwise, we just return the same type
local function resolve_type(type)
  local name = type.n
  local typedef = typedefs[name]

  if typedef == nil then
    return type
  else
    return resolve_type(typedef.type)
  end
end


-- Returns a pointer of this type
local function type_to_pointer(type)
  local tag = {
    t = type,
    tag = "Pointer"
  }
  setmetatable(tag, {
    __tostring = getmetatable(type).__tostring
  })
  return tag
end


local function type_is_number(type)
  type = resolve_type(type)
  local name = type.n
  local inttypes = {
    uint8_t = true,
    uint16_t = true,
    uint32_t = true,
    uint64_t = true,
    int8_t = true,
    int16_t = true,
    int32_t = true,
    int64_t = true,
    size_t = true,
    usize_t = true,
    long = true,
    int = true,

    -- TODO: ... it might be best to use string
    char = true,
  }

  local is_enum = type.tag == "Enum"

  return inttypes[name] == true or is_enum
end


local function type_is_bool(type)
  type = resolve_type(type)
  local name = type.n
  local booltypes = {
    _Bool = true,
    bool = true,
  }
  return booltypes[name] == true
end

local function type_is_userdata(type)
  type = resolve_type(type)
  return type.tag == "Pointer" or type.tag == "Struct" or type.tag == "Union"
end

local function type_is_ellipsis(type)
  return type.ellipsis
end

local function type_is_void(type)
  return resolve_type(type).n == "void"
end



local function write_includes(file)
  file:write("/// Auto generated file\n")
  file:write("\n")
  file:write("#include \"lvgl/lvgl.h\"\n")
  file:write("#include \"lua.h\"\n")
  file:write("#include \"lualib.h\"\n")
  file:write("#include \"lauxlib.h\"\n")
  file:write([[
    // Lua polyfill for luaL_setfuncs and luaL_newlib

    #if defined(LUA_VERSION_NUM) && LUA_VERSION_NUM < 530
      #define luaL_newlibtable(L,l)	\
        lua_createtable(L, 0, sizeof(l)/sizeof((l)[0]) - 1)

      #define luaL_newlib(L,l)  \
        (luaL_newlibtable(L,l), luaL_setfuncs(L,l,0))

      LUALIB_API void luaL_setfuncs (lua_State *L, const luaL_Reg *l, int nup) {
        luaL_checkstack(L, nup, "too many upvalues");
        for (; l->name != NULL; l++) {  /* fill the table with given functions */
          int i;
          for (i = 0; i < nup; i++)  /* copy upvalues to the top */
            lua_pushvalue(L, -nup);
          lua_pushcclosure(L, l->func, nup);  /* closure with those upvalues */
          lua_setfield(L, -(nup + 2), l->name);
        }
        lua_pop(L, nup);  /* remove upvalues */
      }
    #endif
  ]])
  file:write("\n\n")
end

local function write_module_open(file, modules)
  file:write("\n\n")
  file:write("int luaopen_lvgl(lua_State* L) {\n")
  file:write("\tlua_newtable(L);\n")
  file:write("\n")
  file:write("\t/// The root module is a special module for freestanding functions\n")
  file:write("\tluaL_setfuncs(L, lvgl_module_root_table, 0);\n")
  file:write("\n")

  for mname, _ in pairs(modules) do
    if mname ~= "root" then
      file:write(string.format("\tlua_pushstring(L, \"%s\");\n", mname))
      file:write(string.format("\tlvgl_push_module_%s(L);\n", mname))
      file:write("\tlua_settable(L, -3);\n")
      file:write("\n")
    end
  end

  file:write("\treturn 1;\n")
  file:write("}")
end

-- TODO: Error checking
local function write_lua_to_lvgl_type(file, argtype, argname, stackindex)
  file:write("\t");

  local typedecl = cparser.typeToString(argtype, argname)
  if type_is_number(argtype) then
    file:write(string.format(
      "%s = lua_tonumber(L, %d);", typedecl, stackindex
    ))
  elseif type_is_userdata(argtype) then

    local ptrname = "udata_" .. argname
    local ptrdecl = cparser.typeToString(type_to_pointer(argtype), ptrname)
    local ptrtype = cparser.typeToString(type_to_pointer(argtype), "")

    file:write(string.format(
      "%s = (%s) lua_touserdata(L, %d);", ptrdecl, ptrtype, stackindex
    ))
    file:write("\n\t")
    file:write(string.format("%s = *%s;", typedecl, ptrname))
  elseif type_is_bool(argtype) then
    file:write(string.format(
      "%s = lua_toboolean(L, %d);", typedecl, stackindex
    ))
  elseif type_is_ellipsis(argtype) then
    file:write(string.format(
      "// ellipsis"
    ))
  else
    print("Unkown type.")
    print(argtype)
    print("Resolves to: ")
    print(resolve_type(argtype))
    error("write_lua_to_lvgl_type: Unkown Type")
  end

  file:write("\n");
end

local function write_function_wrapper(file, mname, fname, fdata)
  local ftype = fdata.type
  local return_type = ftype.t

  file:write("\n\n")
  file:write(string.format( "static int lvgl_%s_%s_wrapper(lua_State *L) {\n", mname, fname ))

  for i = 1, #ftype do
    local typepair = ftype[i]
    local argtype = typepair[1]
    local argname = typepair[2]

    if type_is_ellipsis(typepair) then
      argtype = typepair
    end

    write_lua_to_lvgl_type(file, argtype, argname, -i)
  end

  file:write("\n\t")

  local has_ret_val = type_is_void(return_type) == false
  if has_ret_val then
    file:write(cparser.typeToString(return_type, "ret"))
    file:write(" = ")
  end


  file:write(fdata.name)
  file:write('(')
  for i = 1, #ftype do
    local argname = ftype[i][2]
    file:write(argname)
    if i ~= #ftype then
      file:write(", ")
    end
  end
  file:write(');\n')


  -- if has_ret_val then

  --   if type is pointer then
  --     if (ret == NULL) {
  --       lua_pushnil
  --     } else {

  --     }
  --   end
  --   -- TODO: Wrap ret val
  -- end


  if has_ret_val then
    file:write("\treturn 1;\n")
  else
    file:write("\treturn 0;\n")
  end
  file:write("}")
end

local function write_module_function_table(file, modules, mname)
  local module = modules[mname]
  file:write(string.format(
    "\n\nstatic const struct luaL_Reg lvgl_module_%s_table[] = {\n",
    mname
  ))

  for fname,fn in pairs(module) do
    file:write(string.format(
      "\t{\"%s\", lvgl_%s_%s_wrapper},\n",
      fname, mname, fname
    ))
  end

  file:write("\t{NULL, NULL}\n};\n\n")
end


local function write_push_module_function(file, modules, mname)
  local module = modules[mname]

  file:write(string.format("void lvgl_push_module_%s(lua_State* L) {\n", mname))
  file:write(string.format("\tluaL_newlib(L, lvgl_module_%s_table);\n", mname))
  file:write("}\n\n")
end


local function emit_lua_module(output_file, res)
  enums = res["enums"]
  defines = res["defines"]
  modules = res["modules"]
  typedefs = res["typedefs"]

  local out_file = io.open(output_file, "w+")

  write_includes(out_file)

  -- Emit Function Wrappers
  for mname, module_functions in pairs(modules) do
    for fname, fdata in pairs(module_functions) do
      write_function_wrapper(out_file, mname, fname, fdata)
    end
  end

  -- Emit Function tables
  for mname, _ in pairs(modules) do
    write_module_function_table(out_file, modules, mname)
  end

  -- Emit module push functions
  for mname, _ in pairs(modules) do
    write_push_module_function(out_file, modules, mname)
  end

  -- Emit module open
  write_module_open(out_file, modules)

  io.close(out_file)
end

return emit_lua_module
