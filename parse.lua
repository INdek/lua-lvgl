local cparser = require 'CParser.cparser'
local io = require 'io'


local function parse_lvgl(rootpath)
  local enums = {}
  local defines = {}
  local modules = {}
  local typedefs = {}

  local function parse_function( decl )

    -- if the string starts with _ its an internal function and we are not interested
    if decl.name:find("^_") then
      return
    end

    -- These functions have weired types, and are not worth porting
    local fn_ignore_list = {
      lv_snprintf = true,
      lv_printf = true,
      lv_vsnprintf = true,

      -- TODO: TMP ignored because of ellipsis
      lv_label_set_text_fmt = true,
    }
    if fn_ignore_list[decl.name] then
      return
    end

    local module, fname = string.match( decl.name, "^lv_?(.-)_(.+)$" )
    if module == "" then
      module = "root"
    end
    if modules[module] == nil then
      modules[module] = {}
    end

    modules[module][fname] = decl
    return
  end


  local opts = {
    '-w',
    '-I' .. rootpath .. 'lvgl',
    '-I' .. rootpath .. 'lvgl/src',
    '-I' .. rootpath .. 'lvgl/src/lv_core',
    '-I' .. rootpath .. 'lvgl/src/lv_draw',
    '-I' .. rootpath .. 'lvgl/src/lv_font',
    '-I' .. rootpath .. 'lvgl/src/lv_gpu',
    '-I' .. rootpath .. 'lvgl/src/lv_hal',
    '-I' .. rootpath .. 'lvgl/src/lv_misc',
    '-I' .. rootpath .. 'lvgl/src/lv_themes',
    '-I' .. rootpath .. 'lvgl/src/lv_widgets',
  }
  local lvgl_h_path = rootpath .. 'lvgl/lvgl.h'
  local iter = cparser.declarationIterator(opts, io.lines(lvgl_h_path), 'test.c')

  for decl in iter do
    if decl.directive == "include" or decl.sclass == "extern" then
      -- ignored
    elseif decl.sclass == "[typetag]" or decl.sclass == "typedef" then
      typedefs[decl.name] = decl
    elseif decl.sclass == "[enum]" then
      table.insert(enums, decl.name)
    elseif decl.directive == "define" then
      table.insert(defines, decl.name)
    elseif decl.type.tag == "Function" then
      parse_function(decl)
    else
      print(decl)
      print(">>", cparser.declToString(decl))
      error("Unrecognized decl")
    end
  end

  return {
    enums = enums,
    defines = defines,
    modules = modules,
    typedefs = typedefs
  }
end


local function parse_drivers(res, rootpath)
  local drivers_module = {}
  res.modules['drivers'] = drivers_module

  local files = {
    'lv_drivers/display/fbdev.h',
    'lv_drivers/display/monitor.h',
    'lv_drivers/display/R61581.h',
    'lv_drivers/display/SHARP_MIP.h',
    'lv_drivers/display/SSD1963.h',
    'lv_drivers/display/ST7565.h',
    'lv_drivers/display/UC1610.h',
    'lv_drivers/gtkdrv/gtkdrv.h',
    'lv_drivers/indev/AD_touch.h',
    'lv_drivers/indev/evdev.h',
    'lv_drivers/indev/FT5406EE8.h',
    'lv_drivers/indev/keyboard.h',
    'lv_drivers/indev/libinput_drv.h',
    'lv_drivers/indev/mouse.h',
    'lv_drivers/indev/mousewheel.h',
    'lv_drivers/indev/XPT2046.h',
  }

  for _, file in ipairs(files) do
    print("File: " .. file)
    local opts = {
      '-w',
      '-I' .. rootpath .. 'lv_drivers',
      '-I' .. rootpath .. 'lv_drivers/display',
      '-I' .. rootpath .. 'lv_drivers/gtkdrv',
      '-I' .. rootpath .. 'lv_drivers/indev',
    }
    local lvgl_h_path = rootpath .. file
    local iter = cparser.declarationIterator(opts, io.lines(lvgl_h_path), 'test.c')

    for decl in iter do
      -- print(decl)
      if decl.directive == "include" or decl.sclass == "extern" then
        print(decl)
      elseif decl.sclass == "[typetag]" or decl.sclass == "typedef" then
        print(decl)
      elseif decl.sclass == "[enum]" then
        print(decl)
      elseif decl.directive == "define" then
        print(decl)
      elseif decl.type.tag == "Function" then
        print(decl)
        -- parse_function(decl)
      else
        print(decl)
        print(">>", cparser.declToString(decl))
        error("Unrecognized decl")
      end
    end
  end





  return res
end


local function parse_file(rootpath)
  local res = {}
  res = parse_lvgl(rootpath)
  res = parse_drivers(res, rootpath)
  return res
end

return parse_file