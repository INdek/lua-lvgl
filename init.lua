local moduleroot = arg and arg[0]
local rootpath = moduleroot:match [[^(.+[\/])[^\/]+$]]
if rootpath and #rootpath > 0 then
  package.path = rootpath..'?.lua;'..package.path
  package.cpath = rootpath..'?.dll;'..package.cpath
else
  rootpath = './'
end

local parse_file = require 'parse'
local emit_lua_module = require 'emit'

local function main()
  local output_file = arg[1] or "out.c"

  local res = parse_file(rootpath)
  emit_lua_module(output_file, res)
end

main()